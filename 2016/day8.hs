module AdventOfCode where

import Data.List.Split (splitOn)
import Data.List (transpose, foldl')

day8Input :: String
day8Input = "rect 1x1\n\
            \rotate row y=0 by 6\n\
            \rect 1x1\n\
            \rotate row y=0 by 3\n\
            \rect 1x1\n\
            \rotate row y=0 by 5\n\
            \rect 1x1\n\
            \rotate row y=0 by 4\n\
            \rect 2x1\n\
            \rotate row y=0 by 5\n\
            \rect 2x1\n\
            \rotate row y=0 by 2\n\
            \rect 1x1\n\
            \rotate row y=0 by 5\n\
            \rect 4x1\n\
            \rotate row y=0 by 2\n\
            \rect 1x1\n\
            \rotate row y=0 by 3\n\
            \rect 1x1\n\
            \rotate row y=0 by 3\n\
            \rect 1x1\n\
            \rotate row y=0 by 2\n\
            \rect 1x1\n\
            \rotate row y=0 by 6\n\
            \rect 4x1\n\
            \rotate row y=0 by 4\n\
            \rotate column x=0 by 1\n\
            \rect 3x1\n\
            \rotate row y=0 by 6\n\
            \rotate column x=0 by 1\n\
            \rect 4x1\n\
            \rotate column x=10 by 1\n\
            \rotate row y=2 by 16\n\
            \rotate row y=0 by 8\n\
            \rotate column x=5 by 1\n\
            \rotate column x=0 by 1\n\
            \rect 7x1\n\
            \rotate column x=37 by 1\n\
            \rotate column x=21 by 2\n\
            \rotate column x=15 by 1\n\
            \rotate column x=11 by 2\n\
            \rotate row y=2 by 39\n\
            \rotate row y=0 by 36\n\
            \rotate column x=33 by 2\n\
            \rotate column x=32 by 1\n\
            \rotate column x=28 by 2\n\
            \rotate column x=27 by 1\n\
            \rotate column x=25 by 1\n\
            \rotate column x=22 by 1\n\
            \rotate column x=21 by 2\n\
            \rotate column x=20 by 3\n\
            \rotate column x=18 by 1\n\
            \rotate column x=15 by 2\n\
            \rotate column x=12 by 1\n\
            \rotate column x=10 by 1\n\
            \rotate column x=6 by 2\n\
            \rotate column x=5 by 1\n\
            \rotate column x=2 by 1\n\
            \rotate column x=0 by 1\n\
            \rect 35x1\n\
            \rotate column x=45 by 1\n\
            \rotate row y=1 by 28\n\
            \rotate column x=38 by 2\n\
            \rotate column x=33 by 1\n\
            \rotate column x=28 by 1\n\
            \rotate column x=23 by 1\n\
            \rotate column x=18 by 1\n\
            \rotate column x=13 by 2\n\
            \rotate column x=8 by 1\n\
            \rotate column x=3 by 1\n\
            \rotate row y=3 by 2\n\
            \rotate row y=2 by 2\n\
            \rotate row y=1 by 5\n\
            \rotate row y=0 by 1\n\
            \rect 1x5\n\
            \rotate column x=43 by 1\n\
            \rotate column x=31 by 1\n\
            \rotate row y=4 by 35\n\
            \rotate row y=3 by 20\n\
            \rotate row y=1 by 27\n\
            \rotate row y=0 by 20\n\
            \rotate column x=17 by 1\n\
            \rotate column x=15 by 1\n\
            \rotate column x=12 by 1\n\
            \rotate column x=11 by 2\n\
            \rotate column x=10 by 1\n\
            \rotate column x=8 by 1\n\
            \rotate column x=7 by 1\n\
            \rotate column x=5 by 1\n\
            \rotate column x=3 by 2\n\
            \rotate column x=2 by 1\n\
            \rotate column x=0 by 1\n\
            \rect 19x1\n\
            \rotate column x=20 by 3\n\
            \rotate column x=14 by 1\n\
            \rotate column x=9 by 1\n\
            \rotate row y=4 by 15\n\
            \rotate row y=3 by 13\n\
            \rotate row y=2 by 15\n\
            \rotate row y=1 by 18\n\
            \rotate row y=0 by 15\n\
            \rotate column x=13 by 1\n\
            \rotate column x=12 by 1\n\
            \rotate column x=11 by 3\n\
            \rotate column x=10 by 1\n\
            \rotate column x=8 by 1\n\
            \rotate column x=7 by 1\n\
            \rotate column x=6 by 1\n\
            \rotate column x=5 by 1\n\
            \rotate column x=3 by 2\n\
            \rotate column x=2 by 1\n\
            \rotate column x=1 by 1\n\
            \rotate column x=0 by 1\n\
            \rect 14x1\n\
            \rotate row y=3 by 47\n\
            \rotate column x=19 by 3\n\
            \rotate column x=9 by 3\n\
            \rotate column x=4 by 3\n\
            \rotate row y=5 by 5\n\
            \rotate row y=4 by 5\n\
            \rotate row y=3 by 8\n\
            \rotate row y=1 by 5\n\
            \rotate column x=3 by 2\n\
            \rotate column x=2 by 3\n\
            \rotate column x=1 by 2\n\
            \rotate column x=0 by 2\n\
            \rect 4x2\n\
            \rotate column x=35 by 5\n\
            \rotate column x=20 by 3\n\
            \rotate column x=10 by 5\n\
            \rotate column x=3 by 2\n\
            \rotate row y=5 by 20\n\
            \rotate row y=3 by 30\n\
            \rotate row y=2 by 45\n\
            \rotate row y=1 by 30\n\
            \rotate column x=48 by 5\n\
            \rotate column x=47 by 5\n\
            \rotate column x=46 by 3\n\
            \rotate column x=45 by 4\n\
            \rotate column x=43 by 5\n\
            \rotate column x=42 by 5\n\
            \rotate column x=41 by 5\n\
            \rotate column x=38 by 1\n\
            \rotate column x=37 by 5\n\
            \rotate column x=36 by 5\n\
            \rotate column x=35 by 1\n\
            \rotate column x=33 by 1\n\
            \rotate column x=32 by 5\n\
            \rotate column x=31 by 5\n\
            \rotate column x=28 by 5\n\
            \rotate column x=27 by 5\n\
            \rotate column x=26 by 5\n\
            \rotate column x=17 by 5\n\
            \rotate column x=16 by 5\n\
            \rotate column x=15 by 4\n\
            \rotate column x=13 by 1\n\
            \rotate column x=12 by 5\n\
            \rotate column x=11 by 5\n\
            \rotate column x=10 by 1\n\
            \rotate column x=8 by 1\n\
            \rotate column x=2 by 5\n\
            \rotate column x=1 by 5"

day8Processed :: [String]
day8Processed = lines day8Input

type Board = [[Bool]]

nullBoard :: Board
nullBoard = [replicate 50 False | _ <- [1..6]]

showBoard :: Board -> String
showBoard = unlines . (map . map $ (\c -> if c==True then '#' else '.'))

boardFromString :: [String] -> Board
boardFromString s = [[if c=='#' then True else False | c <- row] | row <- s]

printBoard :: Board -> IO ()
printBoard = putStrLn . showBoard

type Instruction = Board -> Board

-- | parseInstruction creates an Instruction function out of a String
-- based on the rules defined in the question.
--
-- @"rect AxB"@               -> @onRect A B@
-- @"rotate row y=A by B"@    -> @moveRow A B@
-- @"rotate column x=A by B"@ -> @moveCol A B@
parseInstruction :: String -> Instruction
parseInstruction s | take 4 s              == "rect" = parseRect s
                   | (take 3 . drop 7 $ s) == "row"  = parseRow s
                   | otherwise                       = parseColumn s where
    parseRect s   = let [a, b] = splitOn "x" . drop 5 $ s in onRect (read a) (read b)
    parseRow s    = let (a, b) = parseRotate s in moveRow (read a) (read b)
    parseColumn s = let (a, b) = parseRotate s in moveColumn (read a) (read b)
    parseRotate s = let [a, b] = splitOn " by " . last . splitOn "=" $ s in (a, b)

-- Instructions follow:

-- | @onRect w h board@ turns on a rectangle with width @w@ and height @h@
-- with its top-left corner at the top-left corner of @board@
onRect :: Int -> Int -> Board -> Board
onRect w h board = replacementRows ++ (drop h) board where
  replacementRows = [replicate w True ++ drop w row | row <- take h board]

-- | @moveRow r n@ rotates row @r@ (zero-indexed) of @board@ rightwards @n@
-- places, wrapping back to the left where values exceed the row length
moveRow :: Int -> Int -> Board -> Board
moveRow r n board = concat [take r board, [newRow], drop (r+1) board] where 
  affectedRow = board !! r
  rowLength   = length affectedRow
  modN        = n `mod` rowLength  -- in case @n@ exceeds @rowLength@
  newRow      = let (start, end) = splitAt (rowLength - modN) affectedRow in end ++ start

moveColumn :: Int -> Int -> Board -> Board
-- moveColumn c n board = transpose . map reverse . moveRow c n $ [reverse row | row <- transpose board]
moveColumn c n board = transpose . moveRow c n $ transpose board

countLitPixels :: Board -> Int
countLitPixels board = sum [1 | row <- board, c <- row, c==True]

day8Part1 :: Int
day8Part1 = countLitPixels $ foldl' (\b f -> f b) nullBoard $ map parseInstruction day8Processed

-- part 2

day8Part2 :: String
day8Part2 = showBoard $ foldl' (\b f -> f b) nullBoard $ map parseInstruction day8Processed