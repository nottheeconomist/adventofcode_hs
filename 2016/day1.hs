module AdventOfCode where

-- split
import Data.List.Split (splitOn)
-- base
import Data.List (find, inits)
import Data.Maybe (isJust, maybe)

day1input :: String
day1input = "L4, R2, R4, L5, L3, L1, R4, R5, R1, R3, L3, L2, L2, R5, R1, L1, L2, \
            \R2, R2, L5, R5, R5, L2, R1, R2, L2, L4, L1, R5, R2, R1, R1, L2, L3, \
            \R2, L5, L186, L5, L3, R3, L5, R4, R2, L5, R1, R4, L1, L3, R3, R1, L1, \
            \R4, R2, L1, L4, R5, L1, R50, L4, R3, R78, R4, R2, L4, R3, L4, R4, L1, \
            \R5, L4, R1, L2, R3, L2, R5, R5, L4, L1, L2, R185, L5, R2, R1, L3, R4, \
            \L5, R2, R4, L3, R4, L2, L5, R1, R2, L2, L1, L2, R2, L2, R1, L5, L3, L4, \
            \L3, L4, L2, L5, L5, R2, L3, L4, R4, R4, R5, L4, L2, R4, L5, R3, R1, L1, \
            \R3, L2, R2, R1, R5, L4, R5, L3, R2, R3, R1, R4, L4, R1, R3, L5, L1, L3, \
            \R2, R1, R4, L4, R3, L3, R3, R2, L3, L3, R4, L2, R4, L3, L4, R5, R1, L1, \
            \R5, R3, R1, R3, R4, L1, R4, R3, R1, L5, L5, L4, R4, R3, L2, R1, R5, L3, \
            \R4, R5, L4, L5, R2"

day1Processed :: [String]
day1Processed = splitOn ", " day1input

data Direction = North | East | South | West deriving (Enum, Show)
type Location = (Int, Int)

-- |'day1Apply' takes your current 'Direction' and 'Location', applies the instruction
-- and gives back a tuple of (newDirection, (new, location))
day1Apply :: Direction -> Location -> String -> (Direction, Location)
day1Apply d' loc (t:num') = (d, step d loc numsteps)
  where d        = turn d' t
        numsteps = read num' :: Int

-- |'distanceBetween' returns the taxicab geometric distance between two 'Location's
distanceBetween :: Location -> Location -> Int
distanceBetween (x1, y1) (x2, y2) = (abs $ x1-x2) + (abs $ y1-y2)


-- |'turn' changes direction based on the received Char
turn :: Direction -> Char -> Direction
turn West  'R' = North
turn North 'L' = West
turn d     'R' = succ d
turn d     'L' = pred d
turn d      _  = d

-- |'step' moves location based on current direction and number of steps
step :: Direction -> Location -> Int -> Location
step North (x, y) s = (x  , y+s)
step East  (x, y) s = (x+s, y)
step South (x, y) s = (x  , y-s)
step West  (x, y) s = (x-s, y)

finalLocation :: Location
(_, finalLocation) = foldl (\(d, loc) x -> day1Apply d loc x) (North, (0, 0)) day1Processed

day1Part1 :: Int
day1Part1 = distanceBetween (0, 0) finalLocation

-- part 2

data Path = Path Location Location deriving (Show)

-- | combined inequality. b `between` (a, c) -> a <= b <= c
between :: Ord a => a -> (a, a) -> Bool
between b (a, c) = a <= b && b <= c

-- | intersection gives the location at which two Paths intersect, or Nothing
intersection :: Path -> Path -> Maybe Location
intersection p1 p2 | isPerpindicular p1 p2 && doCross = Just (constantX, constantY)
                   | otherwise                        = Nothing
  where isPerpindicular (Path (xa1, ya1) (xb1, yb1)) (Path (xa2, ya2) (xb2, yb2)) = not $ xa1 == xb1 && xa2 == xb2 || ya1 == yb1 && ya2 == yb2
        verticalLine   = case (p1, p2) of
          ((Path (x1, _) (x2, _)), _) | x1 == x2 -> p1
          (_, (Path (x1, _) (x2, _))) | x1 == x2 -> p2
          _ -> undefined
        horizontalLine = case (p1, p2) of
          ((Path (_, y1) (_, y2)), _) | y1 == y2 -> p1
          (_, (Path (_, y1) (_, y2))) | y1 == y2 -> p2
          _ -> undefined
        constantX      = case verticalLine   of (Path (x, _) _) -> x
        constantY      = case horizontalLine of (Path (_, y) _) -> y
        doCross        = case (horizontalLine, verticalLine) of
          ((Path (xa1, ya1) (xb1, yb1)), (Path (xa2, ya2) (xb2, yb2))) | constantX `between` (xa1, xb1) &&
                                                                         constantY `between` (ya2, yb2) -> True
          _ -> False

locations :: [Location]
locations = map snd $ scanl (\(d, loc) x -> day1Apply d loc x) (North, (0, 0)) day1Processed

paths :: [Path]
paths = case (reverse locations) of
    (l2:l1:ls) -> foldr (\srcloc acc@((Path dstloc _):_) -> (Path srcloc dstloc):acc) [(Path l1 l2)] (reverse ls)


-- | safeInit is 'init', but returns [] on empty list
safeInit :: [a] -> [a]
safeInit [] = []
safeInit xs = init xs

firstRepeat :: [Path] -> Maybe Location
firstRepeat [] = Nothing
firstRepeat xs = go (tail . inits $ xs) where
  go []     = Nothing
  go (x:xs) = maybe (go xs) id $ find isJust $ map (intersection (last x)) (safeInit . init $ x)

day1Part2 :: Int
day1Part2 = maybe (-1) (distanceBetween (0, 0)) $ firstRepeat paths