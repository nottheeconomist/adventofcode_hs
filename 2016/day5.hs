module AdventOfCode where

-- pureMD5
import qualified Data.Digest.Pure.MD5 as MD5
-- base
import qualified Data.ByteString.Builder as BBuild
import Data.ByteString (ByteString)
import Data.Maybe (maybe)
import Data.List (sort, foldl')

day5Input :: String
day5Input = "ojvtpuvg"

day5Processed :: [String]
day5Processed = [day5Input++(show i) | i <- [0..]]

md5 :: String -> MD5.MD5Digest
md5 = MD5.md5 . builder where
  builder = BBuild.toLazyByteString . BBuild.stringUtf8

startsWith :: Eq a => [a] -> [a] -> Bool
startsWith needle haystack = needle == starts where
  starts = take (length needle) haystack

validCodes :: [MD5.MD5Digest] -> [MD5.MD5Digest]
validCodes = filter (startsWith "00000" . show)

day5Part1 :: String
day5Part1 = take 8 [head . (drop 5) . show $ h | h <- map md5 day5Processed, startsWith "00000" . show $ h]

manyValidCodes = take 8 [(i, show h) | (i, h) <- zip [0..] $ map md5 day5Processed, startsWith "00000" . show $ h]

-- part 2

data Password = Password (Maybe Char) (Maybe Char) (Maybe Char) (Maybe Char) (Maybe Char) (Maybe Char) (Maybe Char) (Maybe Char) deriving Show

nullPassword :: Password
nullPassword = Password Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

setByIndex :: Int -> Password -> Char -> Password
setByIndex 0 pwd@(Password Nothing b c d e f g h) ch = Password (Just ch) b c d e f g h
setByIndex 1 pwd@(Password a Nothing c d e f g h) ch = Password a (Just ch) c d e f g h
setByIndex 2 pwd@(Password a b Nothing d e f g h) ch = Password a b (Just ch) d e f g h
setByIndex 3 pwd@(Password a b c Nothing e f g h) ch = Password a b c (Just ch) e f g h
setByIndex 4 pwd@(Password a b c d Nothing f g h) ch = Password a b c d (Just ch) f g h
setByIndex 5 pwd@(Password a b c d e Nothing g h) ch = Password a b c d e (Just ch) g h
setByIndex 6 pwd@(Password a b c d e f Nothing h) ch = Password a b c d e f (Just ch) h
setByIndex 7 pwd@(Password a b c d e f g Nothing) ch = Password a b c d e f g (Just ch)
setByIndex _ pwd                                  _  = pwd

passwordFromString :: String -> Password
passwordFromString s = foldr (\(n, x) pwd -> setByIndex n pwd x) nullPassword (zip [0..7] s)

passwordToString :: Password -> String
passwordToString (Password a b c d e f g h) = map (maybe '_' id) [a, b, c, d, e, f, g, h]

passwordComplete :: Password -> Bool
passwordComplete (Password (Just _) (Just _) (Just _) (Just _) (Just _) (Just _) (Just _) (Just _)) = True
passwordComplete _ = False

validCodeWithPos :: MD5.MD5Digest -> Maybe (Int, Char)
validCodeWithPos h = case (show h) of
    ('0':'0':'0':'0':'0':j:c:_) | j `elem` ['0'..'7'] -> Just (read [j], c)
                                | otherwise           -> Nothing
    _ -> Nothing

crackPassword :: [Maybe (Int, Char)] -> Password
crackPassword = go nullPassword where
  go pwd (x:xs) | passwordComplete pwd = pwd
                | otherwise            = case x of (Just (n, ch)) -> go (setByIndex n pwd ch) xs
                                                   _ -> go pwd xs

possibleDoors :: [Maybe (Int, Char)]
possibleDoors = map (validCodeWithPos . md5) day5Processed

day5Part2 :: String
day5Part2 = passwordToString $ crackPassword possibleDoors