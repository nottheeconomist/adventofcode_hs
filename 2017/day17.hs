module Main where

import           Data.Sequence (Seq(..), (><))
import qualified Data.Sequence as S
import           Data.List     (foldl')

insertAt' :: Int -> a -> Seq a -> Seq a
insertAt' k v s = S.singleton v >< after >< before
  where
  k'              = (k `mod` S.length s) + 1
  (before, after) = S.splitAt k' s

buildSeq :: Int -> [Seq Int]
buildSeq k = f [1..] $ S.singleton 0
  where
  f (v:vs) s = s' : f vs s'
    where s' = insertAt' k v s

findNext :: Int -> Seq Int -> Int
findNext n (x :<| xs) | x == n    = case xs of (x' :<| _ ) -> x'
                      | otherwise = findNext n xs
findNext _ seq = error $ "findNext: Empty Seq! " ++ show seq

solve :: Int -> Int -> Int
solve k x = fst . foldl' (uncurry f) (0, 0) $ [1..x]
  where
  f res i v = let len = v
                  i' = (i + k) `mod` len + 1
              in if i' == 1
                 then (v, i')
                 else (res, i')

main :: IO ()
main = do
  let seqs = buildSeq 370
  print $ findNext 2017 $ seqs !! (2017 - 1)
  -- the above approach fails for very large sequences with a stack overflow.
  print $ solve 370 (50*10^6)