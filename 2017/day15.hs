module Main where

import           Data.Bits (Bits, (.&.))

last16 :: (Num a, Bits a) => a -> a
last16 = (.&. 65535)

match :: (Num a, Bits a) => a -> a -> Bool
match a b = last16 a == last16 b

testGenA, testGenB, testGenA2, testGenB2 :: [Integer]
testGenA = tail . iterate ((`mod` 2147483647) . (*16807)) $ 65
testGenB = tail . iterate ((`mod` 2147483647) . (*48271)) $ 8921
testGenA2 = filter ((==0) . (`mod` 4)) $ testGenA
testGenB2 = filter ((==0) . (`mod` 8)) $ testGenB


genA, genB, genA2, genB2 :: [Integer]
genA = tail . iterate ((`mod` 2147483647) . (*16807)) $ 516
genB = tail . iterate ((`mod` 2147483647) . (*48271)) $ 190
genA2 = filter ((==0) . (`mod` 4)) $ genA
genB2 = filter ((==0) . (`mod` 8)) $ genB

countMatches :: (Integral a, Num a, Bits a) => Int -> [a] -> [a] -> Int
countMatches k xs ys = foldr (\(x, y) i -> if match x y then i+1 else i) 0 . take k . zip xs $ ys

doIt :: (Integral a, Num a, Bits a) => [a] -> [a] -> IO ()
doIt xs ys = do
  print $ countMatches (40 * 10^6) xs ys

doIt2 :: (Integral a, Num a, Bits a) => [a] -> [a] -> IO ()
doIt2 xs ys = do
  print $ countMatches (5 * 10^6) xs ys

main :: IO ()
main = do
  doIt genA genB
  doIt2 genA2 genB2

testMain :: IO ()
testMain = do
  doIt testGenA testGenB
  doIt2 testGenA2 testGenB2
