module Main where

import           Data.Bits       (xor)
import           Data.Char       (ord)
import           Data.List.Split (splitOn)
import           Data.List       (foldl', foldl1')
import           Data.Foldable   (toList)
import           Data.Sequence   ((><), (<|), (|>))
import qualified Data.Sequence   as Seq

data DenseHash = DenseHash [Int]

showHash :: DenseHash -> String
showHash (DenseHash xss) = concatMap toHex xss

rotR :: Int -> Seq.Seq a -> Seq.Seq a
rotR k sq = let len = Seq.length sq
                k'  = k `mod` len
            in (Seq.drop k' sq) >< (Seq.take k' sq)

rotL :: Int -> Seq.Seq a -> Seq.Seq a
rotL k sq = let len = Seq.length sq
                k'  = (len - k) `mod` len
            in (Seq.drop k' sq) >< (Seq.take k' sq)

toHex :: Int -> String
toHex n = padWithZero 2 $ toHex' n
  where
  padWithZero k s = replicate k' '0' ++ s
    where
    k' = k - length s

toHex' :: Int -> String
toHex' n | n >= 16   = let (d, m) = n `divMod` 16
                      in c d : toHex' m
         | otherwise = [c n]
  where
  c 0 = '0'
  c 1 = '1'
  c 2 = '2'
  c 3 = '3'
  c 4 = '4'
  c 5 = '5'
  c 6 = '6'
  c 7 = '7'
  c 8 = '8'
  c 9 = '9'
  c 10 = 'a'
  c 11 = 'b'
  c 12 = 'c'
  c 13 = 'd'
  c 14 = 'e'
  c 15 = 'f'

day10InputPart1 :: [Int]
day10InputPart1 = map read . splitOn "," $ "206,63,255,131,65,80,238,157,254,24,133,2,16,0,1,3"

day10InputPart2 :: [Int]
day10InputPart2 = (map ord $ "206,63,255,131,65,80,238,157,254,24,133,2,16,0,1,3") ++ [17, 31, 73, 47, 23]

day10Seq :: Seq.Seq Int
day10Seq = Seq.fromList [0..255]

testInputPart1 :: [Int]
testInputPart1 = map read . splitOn "," $ "3, 4, 1, 5"

testSeq :: Seq.Seq Int
testSeq = Seq.fromList [0..4]

doShift :: Int -> Seq.Seq a -> Seq.Seq a
doShift k sq = let (preflip, rest) = Seq.splitAt k sq
                   postflip        = Seq.reverse preflip
               in rotR k $ postflip >< rest

doShifts :: Seq.Seq a -> [Int] -> Seq.Seq a
doShifts input shifts = let (sq, _, shifted) = doShifts' input shifts
                        in rotL shifted sq

doShifts' :: Seq.Seq a -> [Int] -> (Seq.Seq a, Int, Int)
doShifts' input shifts = foldl' f (input, 0, 0) shifts
  where
  f (sq, skip, total) k = (rotR skip $ doShift k sq, skip + 1, total + skip + k)

doShiftsNTimes :: Int -> Seq.Seq a -> [Int] -> Seq.Seq a
doShiftsNTimes k input shifts = doShifts input (concat . replicate k $ shifts)

buildSparseHash :: [Int] -> Seq.Seq Int
buildSparseHash = doShiftsNTimes 64 (Seq.fromList [0..255] :: Seq.Seq Int)

buildDenseHash :: Seq.Seq Int -> DenseHash
buildDenseHash sq = DenseHash result
  where
  chunks = (map toList . toList . Seq.chunksOf 16 $ sq)
  result = map (foldl1' xor) chunks

main :: IO ()
main = do
  let part1seq = doShifts day10Seq day10InputPart1
      [a, b]   = toList . Seq.take 2 $ part1seq
  print $ a * b
  let part2seq = buildDenseHash . buildSparseHash $ day10InputPart2
  putStrLn $ showHash part2seq