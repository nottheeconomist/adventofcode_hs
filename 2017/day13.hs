module Main where

data Scanner = Scanner { depth :: Int
                       , range :: Int}
  deriving Show

mkScanner :: String -> Scanner
mkScanner = go []
  where
  go _   []           = error "Invalid scanner token"
  go acc (':':' ':rs) = Scanner (read acc) (read rs)
  go acc (x:xs)       = go (acc++[x]) xs

cycleLength :: Scanner -> Int
cycleLength = (subtract 2) . (*2) . range

heightAtTick :: Int -> Scanner -> Int
heightAtTick n s = stepsDown - stepsUp
  where
  r         = range s
  cl        = cycleLength s
  n'        = n `mod` cl
  stepsUp   = max 0 (n' - r)
  stepsDown = min n' r

testInput :: [Scanner]
testInput = map mkScanner . lines $ input
  where
  input = "0: 3\n\
          \1: 2\n\
          \4: 4\n\
          \6: 4"

day13Input :: [Scanner]
day13Input = map mkScanner . lines $ input
  where
  input = "0: 4\n\
          \1: 2\n\
          \2: 3\n\
          \4: 5\n\
          \6: 6\n\
          \8: 4\n\
          \10: 8\n\
          \12: 6\n\
          \14: 6\n\
          \16: 8\n\
          \18: 8\n\
          \20: 6\n\
          \22: 8\n\
          \24: 9\n\
          \26: 8\n\
          \28: 8\n\
          \30: 12\n\
          \32: 12\n\
          \34: 10\n\
          \36: 12\n\
          \38: 12\n\
          \40: 10\n\
          \42: 12\n\
          \44: 12\n\
          \46: 12\n\
          \48: 12\n\
          \50: 12\n\
          \52: 14\n\
          \54: 14\n\
          \56: 12\n\
          \58: 14\n\
          \60: 14\n\
          \62: 14\n\
          \64: 17\n\
          \66: 14\n\
          \70: 14\n\
          \72: 14\n\
          \74: 14\n\
          \76: 14\n\
          \78: 18\n\
          \82: 14\n\
          \88: 18\n\
          \90: 14"

minDelay :: [Scanner] -> Int
minDelay = go 0
  where
  go k ss | null filteredScanners = k
          | otherwise             = go (k+1) ss
    where
    filteredScanners = filter (\s -> heightAtTick (depth s+k) s == 0) ss

main :: IO ()
main = do
  let filteredScanners = filter (\s -> heightAtTick (depth s) s == 0) day13Input
      totalCost        = foldr (\s i -> i + (depth s) * (range s)) 0 filteredScanners
  print totalCost
  let delay = minDelay day13Input
  print delay

testMain :: IO ()
testMain = do
  let filteredScanners = filter (\s -> heightAtTick (depth s) s == 0) testInput
      totalCost        = foldr (\s i -> i + (depth s) * (range s)) 0 filteredScanners
  print totalCost
  let delay = minDelay testInput
  print delay