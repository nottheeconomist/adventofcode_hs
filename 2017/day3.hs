module AdventOfCode where

import qualified Data.Map.Strict as M
import Data.Maybe (catMaybes)

day3Input :: Int
day3Input = 361527

squareAbove :: Int -> Int
squareAbove = go 1 where
  go n k | n*n >= k   =  n
         | otherwise = go (n+2) k

data Corners = Corners Int Int Int Int deriving Show
data Middles = Middles Int Int Int Int deriving Show

toList :: Middles -> [Int]
toList (Middles a b c d) = [a, b, c, d]

corners :: Int -> Corners
corners x = Corners (sq - (x'-1) * 3) (sq - (x'-1) * 2) (sq - (x'-1)) sq where
  sq = (x*x)
  x' = x

middles :: Int -> Middles
middles n = go . corners $ n where
  go (Corners a b c d) = Middles (a - step) (b - step) (c - step) (d - step)
  step =  (n-1) `div` 2

numSteps :: Int -> Int
numSteps n = (+step) . minimum . map abs $ zipWith (-) (toList $ middles sqA) (repeat n) where
  sqA = squareAbove n
  step = (sqA - 1) `div` 2

p1Day3Result = numSteps day3Input

-- part2

type Grid = M.Map (Int, Int) Int

spiral :: [(Int, Int)]
spiral = (0, 0) : go 1 (0, 0) where
  go  k (x, y) = [(x+i, y) | i <- [1..k]] ++ [(x+k, y+i) | i <- [1..k]] ++ go' (k+1) (x+k, y+k)
  go' k (x, y) = [(x-i, y) | i <- [1..k]] ++ [(x-k, y-i) | i <- [1..k]] ++ go  (k+1) (x-k, y-k)

getNeighbors :: (Int, Int) -> Grid -> [Int]
getNeighbors (x, y) m = catMaybes [m M.!? k | k <- neighbors] where
  neighbors = [(x+i, y+j) | i <- [-1..1], j <- [-1..1], (i, j) /= (0, 0)]

step :: (Int, Int) -> Grid -> (Int, Grid)
step loc m = (sumNeighbors, M.insert loc sumNeighbors m) where
  sumNeighbors = sum . getNeighbors loc $ m

finalResult :: Int
finalResult = go (M.singleton (0, 0) 1) (tail spiral) where
  go m (loc:locs) | result > day3Input = result
                  | otherwise = go m' locs where
    (result, m') = step loc m

p2Day3Result = finalResult