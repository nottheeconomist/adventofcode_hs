-- {-# LANGUAGE RankNTypes #-}

module Main where

-- vector
import qualified Data.Vector.Unboxed         as V
-- containers
import qualified Data.Set                    as S

day6Input :: String
day6Input = "10 3   15  10  5   15  5   15  9   2   5   8   5   2   3   6"

day6Processed :: [Int]
day6Processed = map read . words $ day6Input

step :: V.Vector Int -> V.Vector Int
step vec = let len = V.length vec
               maxidx = V.maxIndex vec
               maxval = V.unsafeIndex vec maxidx
               (k, x) = maxval `divMod` len
               constant = zip [0..len-1] (repeat k)
               remainder = [(i `mod` len, 1) | i <- map (+maxidx) [1..x]]
           in  V.accum (+) (vec V.// [(maxidx, 0)]) (constant ++ remainder)

step' :: S.Set (V.Vector Int) -> V.Vector Int -> (S.Set (V.Vector Int), V.Vector Int)
step' s v = let v' = step v
                s' = S.insert v s
            in (s', v')

solve :: [Int] -> Int
solve xs = let v = V.fromList xs
               s = S.empty
           in go 0 s v
  where
  go i s v | S.member v s = i
           | otherwise = go (i+1) s' v'
    where
    (s', v') = step' s v

-- part 2

solve' :: [Int] -> Int
solve' xs = let v = V.fromList xs
                s = S.empty
           in go 0 s v
  where
  go i s v | S.member v s = go' 1 v v'
           | otherwise = go (i+1) s' v'
    where
    (s', v') = step' s v
    go' i vv v | vv == v = i
               | otherwise = go' (i+1) vv (step v)


main :: IO ()
main = do
  print $ solve day6Processed
  print $ solve' day6Processed
