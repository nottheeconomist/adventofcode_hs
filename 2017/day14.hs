module Main where

import           Data.Bits     (xor)
import           Data.Char     (ord, intToDigit, digitToInt)
import           Data.Sequence ((><), (<|), (|>))
import qualified Data.Sequence as Seq
import           Data.List     (foldl', foldl1', group)
import           Data.Foldable (toList)
import qualified Data.Set      as S
import           Data.Array    (Array)
import qualified Data.Array    as A
import           Data.Ix       (inRange)


data KnotHash = KnotHash [Int]

type Disk = [KnotHash]
type Coord = (Int, Int)
type DiskRepr = Array Coord Char

buildDisk :: String -> Disk
buildDisk = map buildKnotHash . makePoss
  where
  makePoss s = [s++('-':show i) | i <- [0..127]]

mkRepr :: Disk -> DiskRepr
mkRepr dsk = A.array bounds' $ zip [(x, y) | y <- [0..hibound], x <- [0..hibound]] (concatMap showHash dsk)
  where
  hibound = length dsk - 1
  bounds' = ((0, 0), (hibound, hibound))

showHash :: KnotHash -> String
showHash (KnotHash xs) = concatMap (showAsBin . digitToInt) . concatMap showAsHex $ xs

showInBaseWithPadding :: Int -> Int -> Int -> String
showInBaseWithPadding b k n = padWithZero k . reverse $ f n
  where
  f n | n >= b    = let (d, m) = n `divMod` b
                    in intToDigit m : f d
      | otherwise = [intToDigit n]
  padWithZero k s = replicate (k - length s) '0' ++ s

showAsHex :: Int -> String
showAsHex = showInBaseWithPadding 16 2

showAsBin :: Int -> String
showAsBin = showInBaseWithPadding 2 4

rotR :: Int -> Seq.Seq a -> Seq.Seq a
rotR k sq = let len = Seq.length sq
                k'  = k `mod` len
            in (Seq.drop k' sq) >< (Seq.take k' sq)

rotL :: Int -> Seq.Seq a -> Seq.Seq a
rotL k sq = let len = Seq.length sq
                k'  = (len - k) `mod` len
            in (Seq.drop k' sq) >< (Seq.take k' sq)

doShift :: Int -> Seq.Seq a -> Seq.Seq a
doShift k sq = let (preflip, rest) = Seq.splitAt k sq
                   postflip        = Seq.reverse preflip
               in rotR k $ postflip >< rest

doShifts :: Seq.Seq a -> [Int] -> Seq.Seq a
doShifts input shifts = let (sq, _, shifted) = doShifts' input shifts
                        in rotL shifted sq
  where
  doShifts' input shifts = foldl' f (input, 0, 0) shifts
    where
    f (sq, skip, total) k = (rotR skip $ doShift k sq, skip + 1, total + skip + k)

doShiftsNTimes :: Int -> Seq.Seq a -> [Int] -> Seq.Seq a
doShiftsNTimes k input shifts = doShifts input (concat . replicate k $ shifts)

buildKnotHash :: String -> KnotHash
buildKnotHash = buildDenseHash . buildSparseHash . (++[17, 31, 73, 47, 23]) . map ord
  where
  buildSparseHash = doShiftsNTimes 64 (Seq.fromList [0..255] :: Seq.Seq Int)
  buildDenseHash sq = KnotHash result
    where
    chunks = (map toList . toList . Seq.chunksOf 16 $ sq)
    result = map (foldl1' xor) chunks

countUsedSectors :: Disk -> Int
countUsedSectors = sum . map (foldr f 0) . map showHash
  where f '0' i = i
        f '1' i = i+1

countUsedGroups :: Disk -> Int
countUsedGroups dsk = S.size . S.fromList . filter (not.null) . map (flip floodFill repr . fst) . A.assocs $ repr
  where repr = mkRepr dsk

floodFill :: Coord -> DiskRepr -> S.Set Coord
floodFill coord repr = go S.empty [coord] repr
  where
  go seen [] _                 = seen
  go seen (coord:coords) repr
    | S.member coord seen || 
      repr A.! coord == '0' = go seen coords repr
    | otherwise             = go (S.insert coord seen) coords' repr
    where
    getNeighbors (x, y) = filter (inRange $ A.bounds repr) [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
    neighbors           = getNeighbors coord
    coords'             = coords ++ neighbors

testInput :: String
testInput = "flqrgnkx"

day14Input :: String
day14Input = "hwlqcszp"

doIt :: String -> IO ()
doIt s = do
  let disk        = buildDisk s
      usedSectors = countUsedSectors disk
      usedGroups  = countUsedGroups disk
  print usedSectors
  print usedGroups

main :: IO ()
main = doIt day14Input

testMain :: IO ()
testMain = doIt testInput